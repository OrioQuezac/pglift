# SPDX-FileCopyrightText: 2021 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

Site settings:

  $ cat > $TMPDIR/passwords.json << 'EOF'
  > {
  >   "main": {
  >     "postgres": "s3per"
  >   }
  > }
  > EOF
  $ POSTGRES_EXPORTER=$(command -v postgres_exporter || command -v prometheus-postgres-exporter)
  $ cat > $TMPDIR/temboard-signing.key << EOF
  > -----BEGIN PUBLIC KEY-----
  > MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQChaZFhzRuFgNDLgAJ2+WQsVQ75
  > G9UswuVxTfltxP4mc+ou8lyj7Ck73+M3HkFE2r623g6DZcNYCXqpyNpInsBo68kD
  > IDgwHKaQBTPyve0VhNjkJyqoKIC6AJKv/wixEwsHUm/rNU8cXnY7WCNGjCV+JrEm
  > ekHBWP1X4hFfPKcvvwIDAQAB
  > -----END PUBLIC KEY-----
  > EOF
  $ mkdir $TMPDIR/temboard-ssl
  $ python -m trustme -q -d $TMPDIR/temboard-ssl
  $ cat > $TMPDIR/config.json <<EOF
  > {
  >   "cli": {
  >     "log_format": "%(levelname)-4s %(message)s"
  >   },
  >   "prefix": "$TMPDIR",
  >   "run_prefix": "$TMPDIR/run",
  >   "postgresql": {
  >     "auth": {
  >       "local": "md5",
  >       "passfile": "$TMPDIR/.pgpass",
  >       "password_command": [
  >         "jq",
  >         "-r",
  >         ".{instance.name}.{role}",
  >         "$TMPDIR/passwords.json"
  >       ]
  >     },
  >     "backuprole": {
  >       "name": "pgbackrest"
  >     }
  >   },
  >   "pgbackrest": {
  >     "repository": {
  >       "mode": "path",
  >       "path": "$TMPDIR/pgbackrest",
  >       "retention": {
  >         "archive": 3
  >       }
  >     }
  >   },
  >   "powa": {
  >     "role": "pwa"
  >   },
  >   "prometheus": {
  >     "execpath": "$POSTGRES_EXPORTER"
  >   },
  >   "temboard": {
  >     "ui_url": "http://localhost:9999",
  >     "signing_key": "$TMPDIR/temboard-signing.key",
  >     "certificate": {
  >       "ca_cert": "$TMPDIR/temboard-ssl/client.pem",
  >       "cert": "$TMPDIR/temboard-ssl/server.pem",
  >       "key": "$TMPDIR/temboard-ssl/server.key"
  >     },
  >     "execpath": "$(command -v temboard-agent)"
  >   }
  > }
  > EOF

Make sure password_command works:
  $ jq -r ".main.postgres" $TMPDIR/passwords.json
  s3per

  $ export SETTINGS="@$TMPDIR/config.json"
  $ pglift site-settings -o json \
  >   | jq '.postgresql.auth, .prometheus, .run_prefix, .prefix'
  {
    "local": "md5",
    "host": "trust",
    "hostssl": "trust",
    "passfile": "$TMPDIR/.pgpass",
    "password_command": [
      "jq",
      "-r",
      ".{instance.name}.{role}",
      "$TMPDIR/passwords.json"
    ]
  }
  {
    "execpath": ".*postgres[-_]exporter", (re)
    "role": "prometheus",
    "configpath": "$TMPDIR/etc/prometheus/postgres_exporter-{name}.conf",
    "queriespath": null,
    "pid_file": "$TMPDIR/run/prometheus/{name}.pid"
  }
  "$TMPDIR/run"
  "$TMPDIR"

  $ alias pglift="pglift --non-interactive --log-level=info"

  $ pglift site-configure install
  INFO creating temBoard log directory
  INFO creating base pgBackRest configuration directory
  INFO installing base pgBackRest configuration
  INFO creating pgBackRest include directory
  INFO creating pgBackRest repository path
  INFO creating pgBackRest log directory
  INFO creating pgBackRest spool directory
  INFO creating PostgreSQL log directory

  $ trap "pglift --non-interactive instance drop main; \
  >   pglift --non-interactive site-configure uninstall; \
  >   port-for -u postgres; \
  >   port-for -u temboard; \
  >   port-for -u prometheus" \
  >   EXIT

Define ports:

  $ PGPORT=$(port-for postgres)
  $ PGEPORT=$(port-for prometheus)
  $ TBPORT=$(port-for temboard)

Create an instance

  $ pglift instance create main \
  >   --data-checksums \
  >   --auth-host=ident \
  >   --port=$PGPORT --surole-password=s3per \
  >   --pgbackrest-stanza=main --pgbackrest-password='b@ck up!' \
  >   --prometheus-port=$PGEPORT \
  >   --temboard-port=$TBPORT --temboard-password='t3Mb0@rd'
  INFO initializing PostgreSQL
  INFO configuring PostgreSQL authentication
  INFO configuring PostgreSQL
  INFO starting PostgreSQL 1\d-main (re)
  INFO creating role 'temboardagent'
  INFO creating role 'pwa'
  INFO creating role 'prometheus'
  INFO creating role 'pgbackrest'
  INFO altering role 'pgbackrest'
  INFO creating 'powa' database in 1\d\/main (re)
  INFO creating extension 'btree_gist' in database powa
  INFO creating extension 'pg_qualstats' in database powa
  INFO creating extension 'pg_stat_statements' in database powa
  INFO creating extension 'pg_stat_kcache' in database powa
  INFO creating extension 'powa' in database powa
  INFO configuring temboard agent 1\d-main (re)
  INFO configuring Prometheus postgres_exporter 1\d-main (re)
  INFO configuring pgBackRest stanza 'main' for pg1-path=\$TMPDIR\/srv\/pgsql\/1\d\/main\/data (re)
  INFO creating pgBackRest stanza main
  INFO checking pgBackRest configuration for stanza main
  INFO starting temboard-agent 1\d-main (re)
  INFO starting Prometheus postgres_exporter 1\d-main (re)

Error cases for instance operations

  $ pglift instance create main --pgbackrest-stanza=st
  Error: instance already exists
  [1]
  $ pglift instance create 'in/va/lid' --pgbackrest-stanza=xxx
  Usage: pglift instance create [OPTIONS] NAME
  Try 'pglift instance create --help' for help.
  
  Error: Invalid value for 'NAME': Value error, must not contain slashes
  [2]
  $ pglift instance create stdby --standby-for='port 1234' --pgbackrest-stanza=stddy
  Usage: pglift instance create [OPTIONS] NAME
  Try 'pglift instance create --help' for help.
  
  Error: Invalid value for '--standby-for': Value error, missing "=" after "port" in connection info string
  
  [2]
  $ pglift instance apply
  Usage: pglift instance apply [OPTIONS]
  Try 'pglift instance apply --help' for help.
  
  Error: Missing option '-f' / '--file'.
  [2]
  $ pglift instance alter notfound --port=1234
  Usage: pglift instance alter [OPTIONS] [INSTANCE]
  Try 'pglift instance alter --help' for help.
  
  Error: Invalid value for '[INSTANCE]': instance 'notfound' not found
  [2]
  $ pglift instance status notfound
  Usage: pglift instance status [OPTIONS] [INSTANCE]
  Try 'pglift instance status --help' for help.
  
  Error: Invalid value for '[INSTANCE]': instance 'notfound' not found
  [2]
  $ pglift instance drop notfound
  Usage: pglift instance drop [OPTIONS] [INSTANCE]...
  Try 'pglift instance drop --help' for help.
  
  Error: Invalid value for '[INSTANCE]...': instance 'notfound' not found
  [2]

List instances

  $ pglift instance list -o json | jq '.[] | .name, .status'
  "main"
  "running"
  $ pglift instance list --version=14

Get the status of an instance:

  $ pglift --debug instance status main
  DEBUG instance 'main' not found in version 1\d (re)
  DEBUG instance 'main' not found in version 1\d (re)
  DEBUG instance 'main' not found in version 1\d (re)
  DEBUG instance 'main' not found in version 1\d (re)
  DEBUG logging command at \$TMPDIR/log/\d+.\d+.log (re)
  DEBUG looking for 'temboard_agent@1\d-main' service status by its PID at \$TMPDIR\/run\/temboard-agent\/temboard-agent-1\d-main.pid (re)
  DEBUG looking for 'postgres_exporter@1\d-main' service status by its PID at \$TMPDIR\/run\/prometheus\/1\d-main.pid (re)
  DEBUG get status of PostgreSQL instance 1\d\/main (re)
  DEBUG \/usr\/.+1\d\/bin\/pg_ctl --version (re)
  DEBUG \/usr\/.+1\d\/bin\/pg_ctl status -D \$TMPDIR\/srv\/pgsql\/1\d\/main\/data (re)
  PostgreSQL: running
  prometheus: running
  temBoard: running

Reload, restart:

  $ pglift instance reload
  INFO reloading PostgreSQL configuration for 1\d-main (re)
  $ pglift instance restart
  INFO restarting instance 1\d\/main (re)
  INFO stopping temboard-agent 1\d-main (re)
  INFO stopping Prometheus postgres_exporter 1\d-main (re)
  INFO restarting PostgreSQL
  INFO stopping PostgreSQL 1\d-main (re)
  INFO starting PostgreSQL 1\d-main (re)
  INFO starting temboard-agent 1\d-main (re)
  INFO starting Prometheus postgres_exporter 1\d-main (re)

Stop, alter, (re)start an instance:

  $ pglift instance stop
  INFO stopping PostgreSQL 1\d-main (re)
  INFO stopping temboard-agent 1\d-main (re)
  INFO stopping Prometheus postgres_exporter 1\d-main (re)
  $ pglift instance alter --no-data-checksums
  INFO configuring PostgreSQL
  INFO disabling data checksums
  $ pglift instance alter --state=started --powa-password=p0W@
  INFO configuring PostgreSQL
  INFO starting PostgreSQL 1\d-main (re)
  INFO starting temboard-agent 1\d-main (re)
  INFO starting Prometheus postgres_exporter 1\d-main (re)
  $ pglift -Lwarning instance start
  WARNING instance 1\d\/main is already started (re)
  $ pglift instance get -o json \
  >   | jq '.data_checksums, .port, .prometheus, .settings.unix_socket_directories, .state'
  false
  \d+ (re)
  {
    "port": \d+ (re)
  }
  "$TMPDIR/run/postgresql"
  "started"

Instance environment, and program execution:

  $ pglift instance env | grep PASSFILE
  PGPASSFILE=$TMPDIR/.pgpass
  $ pglift instance env -o json | jq '.PGBACKREST_STANZA, .PGHOST, .PGPASSWORD'
  "main"
  "$TMPDIR/run/postgresql"
  "s3per"
  $ pglift instance exec main
  Usage: pglift instance exec [OPTIONS] INSTANCE COMMAND...
  Try 'pglift instance exec --help' for help.
  
  Error: Missing argument 'COMMAND...'.
  [2]
  $ pglift instance exec main -- psql -t -c 'SELECT 1'
          1
  

PostgreSQL configuration

  $ pglift pgconf show port logging_collector
  port = \d+ (re)
  logging_collector = on
  $ pglift pgconf set invalid
  Usage: pglift pgconf set [OPTIONS] <PARAMETER>=<VALUE>...
  Try 'pglift pgconf set --help' for help.
  
  Error: Invalid value for '<PARAMETER>=<VALUE>...': invalid
  [2]
  $ pglift --non-interactive pgconf set log_statement=ddl log_line_prefix=' ~ '
  INFO configuring PostgreSQL
  INFO instance 1\d\/main needs reload due to parameter changes: log_line_prefix, log_statement (re)
  INFO reloading PostgreSQL configuration for 1\d-main (re)
  log_line_prefix: None ->  ~ 
  log_statement: None -> ddl
  $ pglift --non-interactive pgconf set log_statement=ddl
  INFO configuring PostgreSQL
  changes in 'log_statement' not applied
   hint: either these changes have no effect (values already set) or specified parameters are already defined in an un-managed file (e.g. 'postgresql.conf')
  $ pglift pgconf show log_statement
  log_statement = 'ddl'
  $ pglift --non-interactive pgconf remove fsync
  Error: 'fsync' not found in managed configuration
  [1]
  $ pglift --non-interactive pgconf remove log_statement
  INFO configuring PostgreSQL
  INFO instance 1\d\/main needs reload due to parameter changes: log_statement (re)
  INFO reloading PostgreSQL configuration for 1\d-main (re)
  log_statement: ddl -> None
  $ pglift pgconf show log_statement
  # log_statement = 'ddl'

Instance logs:

  $ pglift instance logs --no-follow | grep -v GMT
  INFO reading logs of instance 1\d\/main from \$TMPDIR\/log\/postgresql\/1\d-main-.+.log (re)
   ~ LOG:  parameter "log_line_prefix" changed to " ~ "
   ~ LOG:  parameter "log_statement" changed to "ddl"
   ~ LOG:  received SIGHUP, reloading configuration files
   ~ LOG:  parameter "log_statement" removed from configuration file, reset to default


Roles

Add and manipulate roles:

  $ pglift role -i main create dba --login --pgpass --password=qwerty --in-role=pg_read_all_stats
  INFO creating role 'dba'
  INFO adding an entry for 'dba' in \$TMPDIR\/.pgpass \(port=\d+\) (re)

  $ cat $TMPDIR/.pgpass
  \*:\d+:\*:dba:qwerty (re)

  $ pglift role get dba
   name  has_\xe2\x80\xa6  inhe\xe2\x80\xa6  login  supe\xe2\x80\xa6  crea\xe2\x80\xa6  crea\xe2\x80\xa6  rep\xe2\x80\xa6  conn\xe2\x80\xa6  val\xe2\x80\xa6  in_r\xe2\x80\xa6  pgp\xe2\x80\xa6  (esc)
   dba   True   True   True   False  False  False  Fal\xe2\x80\xa6               pg_r\xe2\x80\xa6  True  (esc)

  $ pglift role -i main create dba
  Error: role already exists
  [1]

  $ PGDATABASE=postgres PGUSER=dba PGPASSWORD= \
  >   pglift instance exec main -- psql -c "SELECT current_user;"
   current_user 
  --------------
   dba
  (1 row)
  

  $ pglift role alter dba --connection-limit=10 --inherit --no-pgpass --no-login
  INFO altering role 'dba'
  INFO removing entry for 'dba' in \$TMPDIR\/.pgpass \(port=\d+\) (re)

  $ pglift role get dba -o json
  {
    "name": "dba",
    "has_password": true,
    "inherit": true,
    "login": false,
    "superuser": false,
    "createdb": false,
    "createrole": false,
    "replication": false,
    "connection_limit": 10,
    "validity": null,
    "in_roles": [
      "pg_read_all_stats"
    ],
    "pgpass": false
  }

  $ cat > $TMPDIR/role.yaml <<EOF
  > name: test
  > connection_limit: 3
  > login: false
  > in_roles: ['pg_monitor']
  > EOF
  $ pglift role apply -f $TMPDIR/role.yaml
  INFO creating role 'test'
  $ cat >> $TMPDIR/role.yaml <<EOF
  > createdb: true
  > EOF
  $ pglift role apply -f $TMPDIR/role.yaml
  INFO altering role 'test'
  $ pglift role get test -o json
  {
    "name": "test",
    "has_password": false,
    "inherit": true,
    "login": false,
    "superuser": false,
    "createdb": true,
    "createrole": false,
    "replication": false,
    "connection_limit": 3,
    "validity": null,
    "in_roles": [
      "pg_monitor"
    ],
    "pgpass": false
  }

  $ pglift role list -o json
  [
    {
      "name": "dba",
      "has_password": true,
      "inherit": true,
      "login": false,
      "superuser": false,
      "createdb": false,
      "createrole": false,
      "replication": false,
      "connection_limit": 10,
      "validity": null,
      "in_roles": [
        "pg_read_all_stats"
      ]
    },
    {
      "name": "pgbackrest",
      "has_password": true,
      "inherit": true,
      "login": true,
      "superuser": true,
      "createdb": false,
      "createrole": false,
      "replication": false,
      "connection_limit": null,
      "validity": null,
      "in_roles": []
    },
    {
      "name": "postgres",
      "has_password": true,
      "inherit": true,
      "login": true,
      "superuser": true,
      "createdb": true,
      "createrole": true,
      "replication": true,
      "connection_limit": null,
      "validity": null,
      "in_roles": []
    },
    {
      "name": "prometheus",
      "has_password": false,
      "inherit": true,
      "login": true,
      "superuser": false,
      "createdb": false,
      "createrole": false,
      "replication": false,
      "connection_limit": null,
      "validity": null,
      "in_roles": [
        "pg_monitor"
      ]
    },
    {
      "name": "pwa",
      "has_password": false,
      "inherit": true,
      "login": true,
      "superuser": true,
      "createdb": false,
      "createrole": false,
      "replication": false,
      "connection_limit": null,
      "validity": null,
      "in_roles": []
    },
    {
      "name": "temboardagent",
      "has_password": true,
      "inherit": true,
      "login": true,
      "superuser": true,
      "createdb": false,
      "createrole": false,
      "replication": false,
      "connection_limit": null,
      "validity": null,
      "in_roles": []
    },
    {
      "name": "test",
      "has_password": false,
      "inherit": true,
      "login": false,
      "superuser": false,
      "createdb": true,
      "createrole": false,
      "replication": false,
      "connection_limit": 3,
      "validity": null,
      "in_roles": [
        "pg_monitor"
      ]
    }
  ]
  $ pglift role get notfound
  Error: role 'notfound' not found
  [1]
  $ pglift role drop notfound
  Error: role 'notfound' not found
  [1]

Databases

  $ pglift database create test --owner test
  INFO creating 'test' database in 1\d\/main (re)
  $ pglift database create myapp --owner dba --schema app
  INFO creating 'myapp' database in 1\d\/main (re)
  INFO creating schema 'app' in database myapp

  $ pglift database create other
  INFO creating 'other' database in 1\d/main (re)
  $ cat > $TMPDIR/other.yaml <<EOF
  > name: other
  > extensions:
  >   - name: pg_stat_statements
  > EOF
  $ pglift database apply -f $TMPDIR/other.yaml --dry-run
  $ pglift database apply -f $TMPDIR/other.yaml -o json
  INFO altering 'other' database on instance 1\d\/main (re)
  INFO creating extension 'pg_stat_statements' in database other
  {
    "change_state": "changed"
  }
  $ pglift database get other
   name   owner     settings  publications  subscriptions  tablespace 
   other  postgres                                         pg_default 
  $ pglift database create other
  Error: database already exists
  [1]

(issue https://gitlab.com/dalibo/pglift/-/issues/389)

  $ pglift database alter other --owner=test
  Error: 1 validation error for Database
  extensions.0.schema_
    Extra inputs are not permitted [type=extra_forbidden, input_value='public', input_type=str]
      .* (re)
  [1]

  $ pglift database get nosuchdb
  Error: database 'nosuchdb' not found
  [1]
  $ pglift database get myapp -o json
  {
    "name": "myapp",
    "owner": "dba",
    "settings": null,
    "schemas": [
      {
        "name": "app",
        "owner": "postgres"
      },
      {
        "name": "public",
        "owner": ".+" (re)
      }
    ],
    "extensions": [],
    "publications": [],
    "subscriptions": [],
    "tablespace": "pg_default"
  }

  $ pglift database run -d myapp \
  >     "CREATE SCHEMA hollywood CREATE TABLE films (title text, release date, awards text[]) CREATE VIEW winners AS SELECT title, release FROM films WHERE awards IS NOT NULL;"
  INFO running "CREATE SCHEMA hollywood CREATE TABLE films \(title text, release date, awards text\[\]\) CREATE VIEW winners AS SELECT title, release FROM films WHERE awards IS NOT NULL;" on myapp database of 1\d\/main (re)
  INFO CREATE SCHEMA

  $ pglift database run -d myapp \
  >     "ALTER DEFAULT PRIVILEGES IN SCHEMA hollywood GRANT ALL ON TABLES TO dba"
  INFO running "ALTER DEFAULT PRIVILEGES IN SCHEMA hollywood GRANT ALL ON TABLES TO dba" on myapp database of 1\d\/main (re)
  INFO ALTER DEFAULT PRIVILEGES

  $ pglift instance privileges main --default -o json
  [
    {
      "database": "myapp",
      "schema": "hollywood",
      "object_type": "TABLE",
      "role": "dba",
      "privileges": [
        "DELETE",
        "INSERT",
        "REFERENCES",
        "SELECT",
        "TRIGGER",
        "TRUNCATE",
        "UPDATE"
      ]
    }
  ]
  $ pglift instance privileges main -d myapp -r dba --default -o json
  [
    {
      "database": "myapp",
      "schema": "hollywood",
      "object_type": "TABLE",
      "role": "dba",
      "privileges": [
        "DELETE",
        "INSERT",
        "REFERENCES",
        "SELECT",
        "TRIGGER",
        "TRUNCATE",
        "UPDATE"
      ]
    }
  ]
  $ pglift instance privileges main -d postgres --default -o json
  []
  $ pglift database privileges myapp -r dba
  $ pglift database privileges myapp --default -o json
  [
    {
      "database": "myapp",
      "schema": "hollywood",
      "object_type": "TABLE",
      "role": "dba",
      "privileges": [
        "DELETE",
        "INSERT",
        "REFERENCES",
        "SELECT",
        "TRIGGER",
        "TRUNCATE",
        "UPDATE"
      ]
    }
  ]
  $ pglift role privileges dba -o json
  []
  $ pglift role privileges dba --default -d myapp -o json
  [
    {
      "database": "myapp",
      "schema": "hollywood",
      "object_type": "TABLE",
      "role": "dba",
      "privileges": [
        "DELETE",
        "INSERT",
        "REFERENCES",
        "SELECT",
        "TRIGGER",
        "TRUNCATE",
        "UPDATE"
      ]
    }
  ]

  $ pglift database run -d myapp "bad sql"
  INFO running "bad sql" on myapp database of 1\d\/main (re)
  Error: syntax error at or near "bad"
  LINE 1: bad sql
          ^
  [1]
  $ pglift database run -d myapp \
  >     "INSERT INTO hollywood.films VALUES ('Blade Runner', 'June 25, 1982', '{\"Hugo Award\", \"Saturn Award\"}');"
  INFO running "INSERT INTO hollywood.films VALUES \('Blade Runner', 'June 25, 1982', '{"Hugo Award", "Saturn Award"}'\);" on myapp database of 1\d\/main (re)
  INFO INSERT 0 1
  $ pglift database run -d myapp -o json "TABLE hollywood.films;"
  INFO running "TABLE hollywood.films;" on myapp database of 1\d\/main (re)
  INFO SELECT 1
  {
    "myapp": [
      {
        "title": "Blade Runner",
        "release": "1982-06-25",
        "awards": [
          "Hugo Award",
          "Saturn Award"
        ]
      }
    ]
  }

  $ PGDATABASE=myapp pglift instance exec main -- \
  >     psql -c "SELECT * FROM hollywood.winners ORDER BY release ASC;"
      title     |  release   
  --------------+------------
   Blade Runner | 1982-06-25
  (1 row)
  

  $ pglift database dump nosuchdb
  INFO backing up database 'nosuchdb' on instance 1\d\/main (re)
  Error: .+ database "nosuchdb" does not exist (re)
  [1]
  $ pglift database dump myapp
  INFO backing up database 'myapp' on instance 1\d\/main (re)

  $ pglift database list -o json | jq '.[] | .name, .owner, .description'
  "myapp"
  "dba"
  null
  "other"
  "postgres"
  null
  "postgres"
  "postgres"
  "default administrative connection database"
  "powa"
  "postgres"
  null
  "template1"
  "postgres"
  "default template for new databases"
  "test"
  "test"
  null
  $ pglift database list template1 -o json | jq '.[] | .encoding, .collation, .tablespace.name'
  "UTF8"
  "C"
  "pg_default"
  $ pglift database drop nosuchdb
  Error: database 'nosuchdb' not found
  [1]
  $ pglift database drop myapp
  INFO dropping 'myapp' database

  $ pglift role drop test
  INFO dropping role 'test'
  Error: role "test" cannot be dropped because some objects depend on it (detail: owner of database test)
  [1]
  $ pglift role drop test --drop-owned --reassign-owned=postgres
  Usage: pglift role drop [OPTIONS] NAME
  Try 'pglift role drop --help' for help.
  
  Error: Invalid value for '--reassign-owned': Value error, drop_owned and reassign_owned are mutually exclusive
  [2]
  $ pglift role drop test --drop-owned
  INFO dropping role 'test'

Cleanup.
  INFO dropping instance 1\d\/main (re)
  INFO stopping PostgreSQL 1\d-main (re)
  INFO stopping temboard-agent 1\d-main (re)
  INFO stopping Prometheus postgres_exporter 1\d-main (re)
  INFO deconfiguring temboard agent
  INFO deconfiguring Prometheus postgres_exporter 1\d-main (re)
  INFO deconfiguring pgBackRest
  INFO removing entries matching port=\d+ from \$TMPDIR\/.pgpass (re)
  INFO removing now empty $TMPDIR/.pgpass
  INFO deleting PostgreSQL cluster
  INFO deleting temBoard log directory
  INFO deleting pgBackRest include directory
  INFO uninstalling base pgBackRest configuration
  INFO deleting pgBackRest log directory
  INFO deleting pgBackRest spool directory
  INFO deleting PostgreSQL log directory (no-eol)
