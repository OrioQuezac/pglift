# SPDX-FileCopyrightText: 2024 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

import enum
from datetime import date
from typing import Annotated, Literal, Optional

from pydantic import AfterValidator, BaseModel, Field, SecretStr, ValidationInfo

from pglift.types import AnsibleConfig, AutoStrEnum, CLIConfig


class Gender(enum.Enum):
    male = "M"
    female = "F"


CountryValues = Literal["fr", "be", "gb"]


class Location(BaseModel):
    system: Annotated[Literal["4326"], Field(description="coordinates system")] = "4326"
    long_: Annotated[float, Field(alias="long", description="longitude")]
    lat: Annotated[float, Field(description="latitude")]


def validate_city_and_country(
    value: CountryValues, info: ValidationInfo
) -> CountryValues:
    if value == "fr" and info.data["city"] == "bruxelles":
        raise ValueError("Bruxelles is in Belgium!")
    return value


class Address(BaseModel, extra="forbid"):
    street: Annotated[list[str], Field(description="street lines", min_length=1)]
    building: Annotated[
        Optional[str], CLIConfig(hide=True), AnsibleConfig(hide=True)
    ] = None
    zip_code: Annotated[
        int, AnsibleConfig(hide=True), Field(description="ZIP code")
    ] = 0
    city: Annotated[
        str,
        CLIConfig(name="town", metavar="city"),
        AnsibleConfig(spec={"type": "str", "description": ["the city"]}),
        Field(description="city"),
    ]
    country: Annotated[
        CountryValues,
        AfterValidator(validate_city_and_country),
        CLIConfig(choices=["fr", "be"]),
        AnsibleConfig(choices=["fr", "gb"]),
    ]
    primary: Annotated[bool, Field(description="is this person's primary address?")] = (
        False
    )
    coords: Annotated[Optional[Location], Field(description="coordinates")] = None


class Title(AutoStrEnum):
    mr = enum.auto()
    ms = enum.auto()
    dr = enum.auto()


class PhoneNumber(BaseModel):
    label: Annotated[str, Field(description="Type of phone number")]
    number: Annotated[str, Field(description="Number")]


class BirthInformation(BaseModel):
    date_: Annotated[date, Field(alias="date", description="date of birth")]
    place: Annotated[
        Optional[str],
        Field(description="place of birth", json_schema_extra={"readOnly": True}),
    ] = None


class Person(BaseModel, extra="forbid"):
    name: Annotated[str, Field(min_length=3)]
    nickname: Optional[SecretStr] = None
    gender: Optional[Gender] = None
    title: list[Title] = []
    age: Annotated[Optional[int], Field(description="age")] = None
    address: Optional[Address] = None
    birth: Annotated[BirthInformation, Field(description="birth information")]
    phone_numbers: Annotated[
        list[PhoneNumber],
        Field(
            default_factory=list,
            description="Phone numbers",
        ),
    ]
