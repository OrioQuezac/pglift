# SPDX-FileCopyrightText: 2021 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

from __future__ import annotations

import pytest

from pglift import databases, exceptions
from pglift.models.interface import Database, DatabaseDropped
from pglift.models.system import Instance


@pytest.mark.anyio
async def test_standby_database_apply(standby_instance: Instance) -> None:
    with pytest.raises(
        exceptions.InstanceReadOnlyError,
        match=f"^{standby_instance.version}/standby is a read-only standby instance$",
    ):
        await databases.apply(standby_instance, Database(name="test"))


@pytest.mark.anyio
async def test_standby_database_drop(standby_instance: Instance) -> None:
    with pytest.raises(
        exceptions.InstanceReadOnlyError,
        match=f"^{standby_instance.version}/standby is a read-only standby instance$",
    ):
        await databases.drop(standby_instance, DatabaseDropped(name="test"))
