# SPDX-FileCopyrightText: 2021 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

from __future__ import annotations

import pytest

from pglift import exceptions, roles
from pglift.models import interface
from pglift.models.system import Instance


@pytest.mark.anyio
async def test_standby_role_drop(standby_instance: Instance) -> None:
    role = interface.Role(name="alice")
    with pytest.raises(
        exceptions.InstanceReadOnlyError,
        match=f"^{standby_instance.version}/standby is a read-only standby instance$",
    ):
        await roles.drop(standby_instance, role)
