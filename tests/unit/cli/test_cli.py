# SPDX-FileCopyrightText: 2021 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

from __future__ import annotations

import io
import json
import logging
import re
from collections.abc import Iterator, Sequence
from contextlib import contextmanager
from dataclasses import asdict, dataclass
from datetime import date
from pathlib import Path
from typing import Annotated, Any
from unittest.mock import MagicMock, Mock, patch

import click
import pydantic
import pytest
import yaml
from click.shell_completion import ShellComplete
from click.testing import CliRunner
from rich.console import Console, ConsoleDimensions

from pglift import cmd, exceptions, instances, postgresql, ui
from pglift._compat import zip
from pglift.cli import _site, hookspecs
from pglift.cli import instance as instance_cli
from pglift.cli import patroni, pm, postgres, prometheus, util
from pglift.cli.main import cli
from pglift.cli.main import console as cli_console
from pglift.cli.util import (
    Command,
    Obj,
    async_command,
    get_instance,
    instance_identifier,
    instance_identifier_option,
    pass_instance,
)
from pglift.models import interface, system
from pglift.models.system import Instance
from pglift.settings import Settings
from pglift.types import ByteSizeType, Status

from . import click_result_traceback


def assert_mock_call(fn: Mock, *expected_args: Any, **expected_kwargs: Any) -> None:
    """Check mock calls by dumping pydantic models as they are not comparable."""
    if fn.call_count != 1:
        pytest.fail(f"expected {fn} to be called once, called {fn.call_count}")
    args, kwargs = fn.call_args
    for idx, (a, ea) in enumerate(zip(args, expected_args, strict=True), start=1):
        if isinstance(a, pydantic.BaseModel):
            a = a.model_dump()
        if isinstance(ea, pydantic.BaseModel):
            ea = ea.model_dump()
        if a != ea:
            pytest.fail(f"argument {idx} for {fn} differ: {a} != {ea}")
    if kwargs != expected_kwargs:
        pytest.fail(f"keyword arguments for {fn} differ: {kwargs} != {expected_kwargs}")


instance_arg_guessed_or_given = pytest.mark.parametrize(
    "args", [[], ["test"]], ids=["instance:guessed", "instance:given"]
)


@pytest.fixture
def installed(settings: Settings) -> Iterator[None]:
    with patch("pglift.install.check", return_value=True, autospec=True) as check:
        yield None
    check.assert_called_once_with(settings)


@pytest.fixture(autouse=True)
def _no_command_runners() -> Iterator[None]:
    with patch.object(
        cmd,
        "asyncio_run",
        side_effect=lambda *args, **kwargs: pytest.fail(
            "unexpected call to command runner"
        ),
    ):
        yield None


@pytest.fixture
def runner() -> CliRunner:
    return CliRunner(mix_stderr=False)


@pytest.fixture(autouse=True)
def _settings_globals(
    settings: Settings,
    default_settings: Settings,
    composite_instance_model: type[interface.Instance],
    composite_role_model: type[interface.Role],
) -> Iterator[None]:
    plugin_manager = pm.PluginManager.get(settings, hookspecs)
    with (
        patch.object(_site, "SETTINGS", new=settings),
        patch.object(_site, "DEFAULT_SETTINGS", new=default_settings),
        patch.object(_site, "PLUGIN_MANAGER", new=plugin_manager),
        patch.object(_site, "INSTANCE_MODEL", new=composite_instance_model),
        patch.object(_site, "ROLE_MODEL", new=composite_role_model),
    ):
        yield


@pytest.fixture
def obj() -> Obj:
    return Obj()


@contextmanager
def set_console_width(width: int) -> Iterator[None]:
    old_size = cli_console.size
    cli_console.size = ConsoleDimensions(width, old_size.height)
    try:
        yield
    finally:
        cli_console.size = ConsoleDimensions(old_size.width, old_size.height)


@pytest.fixture
def memory_console() -> Console:
    file = io.StringIO()
    return Console(file=file)


@pytest.fixture
def postgresql_running(instance: Instance) -> Iterator[MagicMock]:
    with patch("pglift.postgresql.running", autospec=True) as m:
        yield m
    m.assert_called_once_with(instance)


@click.command(cls=Command)
@click.argument("error")
@click.pass_context
def mycmd(context: click.Context, error: str) -> None:
    if error == "error":
        raise exceptions.CommandError(1, ["bad", "cmd"], "output", "errs")
    if error == "cancel":
        raise exceptions.Cancelled("flop")
    if error == "runtimeerror":
        raise RuntimeError("oups")
    if error == "exit":
        context.exit(1)


@pytest.mark.parametrize(
    "value, annotations, expected",
    [
        (1024, [ByteSizeType()], "1.0 kB"),
        (1024, (), "1024"),
        ([None, 1, "foo"], (), "None, 1, foo"),
        ({"z", "b", "a"}, (), "a, b, z"),
        (None, (), ""),
        ({"foo": "bob"}, (), "foo: bob"),
        (
            {"foo": "bob", "bar": {"blah": ["some", 123]}},
            (),
            "\n".join(
                [
                    "foo: bob",
                    "bar:",
                    "  blah: some, 123",
                ]
            ),
        ),
    ],
)
def test_prettify(value: Any, annotations: Sequence[Any], expected: str) -> None:
    assert util.prettify(value, annotations) == expected


def test_print_table_for(memory_console: Console) -> None:
    @dataclass
    class Person:
        name: str
        children: list[str]
        address: dict[str, Any]

    items = [
        Person(
            name="bob",
            children=["marley", "dylan"],
            address=dict(street="main street", zip=31234, city="luz"),
        ),
        Person(
            name="janis",
            children=[],
            address=dict(street="robinson lane", zip=38650, city="mars"),
        ),
    ]
    util.print_table_for(items, asdict, title="address book", console=memory_console)
    memory_console.file.seek(0)
    output = memory_console.file.read()
    assert (
        output
        == "\n".join(
            [
                "                  address book                   ",
                "┏━━━━━━━┳━━━━━━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━┓",
                "┃ name  ┃ children      ┃ address               ┃",
                "┡━━━━━━━╇━━━━━━━━━━━━━━━╇━━━━━━━━━━━━━━━━━━━━━━━┩",
                "│ bob   │ marley, dylan │ street: main street   │",
                "│       │               │ zip: 31234            │",
                "│       │               │ city: luz             │",
                "│ janis │               │ street: robinson lane │",
                "│       │               │ zip: 38650            │",
                "│       │               │ city: mars            │",
                "└───────┴───────────────┴───────────────────────┘",
            ]
        )
        + "\n"
    )


class Foo(pydantic.BaseModel):
    bar_: str = pydantic.Field(alias="bar")
    baz: Annotated[
        int, pydantic.PlainSerializer(float, return_type=float, when_used="json")
    ]


@pytest.mark.parametrize(
    "data, expected",
    [
        (
            [Foo(bar="x", baz=1), Foo(bar="y", baz=3)],
            [
                {"bar": "x", "baz": 1.0},
                {"bar": "y", "baz": 3.0},
            ],
        ),
        (
            {
                "name": "p",
                "coords": {"x": 1.2, "y": 3.1415},
                "labels": ("a",),
                "date": date(2021, 10, 27),
            },
            {
                "name": "p",
                "coords": {"x": 1.2, "y": 3.1415},
                "labels": ["a"],
                "date": "2021-10-27",
            },
        ),
    ],
)
def test_print_json_for(data: Any, expected: Any, memory_console: Console) -> None:
    util.print_json_for(data, console=memory_console)
    memory_console.file.seek(0)
    assert json.load(memory_console.file) == expected


def test_yaml_site_settings_error(tmp_path: Path, site_settings: MagicMock) -> None:
    configdir = tmp_path / "pglift"
    configdir.mkdir()
    settings_fpath = configdir / "settings.yaml"
    settings_fpath.touch()
    site_settings.return_value = settings_fpath
    with pytest.raises(click.ClickException, match="expecting an object"):
        _site._load_settings()


@pytest.mark.parametrize(
    "debug, logpath_exists",
    [
        pytest.param(True, False, id="debug:true, logpath_exists:false"),
        pytest.param(False, True, id="debug:false, logpath_exists:true"),
    ],
    ids=lambda v: f"debug: {v[0]}, logpath_exists: {v[1]}",
)
def test_command_error(
    settings: Settings,
    runner: CliRunner,
    obj: Obj,
    caplog: pytest.LogCaptureFixture,
    debug: bool,
    logpath_exists: bool,
) -> None:
    obj.debug = debug
    logpath = settings.cli.logpath
    if logpath_exists:
        logpath.mkdir()
    with caplog.at_level(logging.DEBUG, logger="pglift"):
        result = runner.invoke(mycmd, ["error"], obj=obj)
    if debug:
        (__, __, record, __) = caplog.records
        assert record.exc_text and record.exc_text.startswith(
            "Traceback (most recent call last):"
        )
    else:
        (__, record) = caplog.records
        assert record.exc_text is None
    assert result.exit_code == 1
    assert (
        result.stderr
        == "Error: Command '['bad', 'cmd']' returned non-zero exit status 1.\nerrs\noutput\n"
    )
    assert not list(logpath.glob("*.log"))
    if logpath_exists:
        assert logpath.exists()
    else:
        assert not logpath.exists()


def test_command_cancelled(settings: Settings, runner: CliRunner, obj: Obj) -> None:
    result = runner.invoke(mycmd, ["cancel"], obj=obj)
    assert result.exit_code == 1
    assert result.stderr == "Aborted!\n"
    logpath = settings.cli.logpath
    assert not list(logpath.glob("*.log"))


def test_command_exit(settings: Settings, runner: CliRunner, obj: Obj) -> None:
    result = runner.invoke(mycmd, ["exit"], obj=obj)
    assert result.exit_code == 1
    assert not result.stdout
    logpath = settings.cli.logpath
    assert not list(logpath.glob("*.log"))


def test_command_internal_error(
    settings: Settings, runner: CliRunner, obj: Obj
) -> None:
    result = runner.invoke(mycmd, ["runtimeerror"], obj=obj)
    assert result.exit_code == 1
    logpath = settings.cli.logpath
    logfile = next(logpath.glob("*.log"))
    logcontent = logfile.read_text()
    assert "an unexpected error occurred" in logcontent
    assert "Traceback (most recent call last):" in logcontent
    assert "RuntimeError: oups" in logcontent


@click.command("testasync", cls=Command)
@click.argument("arg")
@async_command
async def asynccmd(arg: str) -> None:
    click.echo(f"called async with {arg}")


def test_asynccommand(runner: CliRunner, obj: Obj) -> None:
    result = runner.invoke(asynccmd, ["value"], obj=obj)
    assert result.exit_code == 0
    assert result.output == "called async with value\n"


def test_get_instance(settings: Settings, instance: Instance) -> None:
    assert get_instance(instance.name, instance.version, settings) == instance

    assert get_instance(instance.name, None, settings) == instance

    with pytest.raises(click.BadParameter):
        get_instance("notfound", None, settings)

    with pytest.raises(click.BadParameter):
        get_instance("notfound", instance.version, settings)

    with patch.object(Instance, "system_lookup", autospec=True) as system_lookup:
        with pytest.raises(
            click.BadParameter,
            match="instance 'foo' exists in several PostgreSQL version",
        ):
            get_instance("foo", None, settings)
    assert system_lookup.call_count == 2


def test_instance_identifier(runner: CliRunner, obj: Obj, instance: Instance) -> None:
    @click.command(cls=Command)
    @instance_identifier(nargs=1)
    def one(instance: system.Instance) -> None:
        """One"""
        click.echo(instance, nl=False)

    @click.command(cls=Command)
    @instance_identifier(nargs=-1)
    def many(instance: tuple[system.Instance]) -> None:
        """Many"""
        click.echo(", ".join(str(i) for i in instance), nl=False)

    @click.command(cls=Command)
    @instance_identifier(nargs=1, required=True)
    def one_required(instance: system.Instance) -> None:
        """One INSTANCE required"""
        click.echo("instance is REQUIRED", nl=False)

    result = runner.invoke(one, [], obj=obj)
    assert result.exit_code == 0, result.stderr
    assert result.stdout == str(instance)

    result = runner.invoke(many, [], obj=obj)
    assert result.exit_code == 0, result.stderr
    assert result.stdout == str(instance)

    result = runner.invoke(one, [str(instance)], obj=obj)
    assert result.exit_code == 0, result.stderr
    assert result.stdout == str(instance)

    result = runner.invoke(many, [str(instance), instance.name], obj=obj)
    assert result.exit_code == 0, result.stderr
    assert result.stdout == f"{instance}, {instance}"

    result = runner.invoke(one_required, [], obj=obj)
    assert result.exit_code == 2
    assert "Missing argument 'INSTANCE'." in result.stderr

    result = runner.invoke(one_required, [instance.name], obj=obj)
    assert result.exit_code == 0, result.stderr
    assert result.stdout == "instance is REQUIRED"


def test_instance_identifier_all_instances(
    runner: CliRunner, obj: Obj, instance: Instance, instance2: Instance
) -> None:
    @click.command(cls=Command)
    @instance_identifier(nargs=-1)
    @click.option("--all", "all_instances", is_flag=True)
    def all(instance: tuple[system.Instance], all_instances: bool) -> None:
        """All"""
        click.echo(", ".join(str(i) for i in instance), nl=False)

    result = runner.invoke(all, [str(instance)], obj=obj)
    assert result.exit_code == 0, result.stderr
    assert result.stdout == str(instance)

    result = runner.invoke(all, [str(instance), instance2.name], obj=obj)
    assert result.exit_code == 0, result.stderr
    assert result.stdout == f"{instance}, {instance2}"

    result = runner.invoke(all, ["--all"], obj=obj)
    assert result.exit_code == 0, result.stderr
    assert result.stdout == f"{instance}, {instance2}"


@pytest.fixture
def cli_app() -> click.Group:
    @click.group
    @instance_identifier_option
    def app(**kwargs: Any) -> None:
        assert kwargs.pop("instance") is None
        assert not kwargs

    @app.command
    @pass_instance
    def cmd(instance: system.Instance) -> None:
        print(f"command on {instance.name!r} ({instance.version})")

    return app


def test_instance_identifier_option_missing(
    runner: CliRunner, obj: Obj, cli_app: click.Group
) -> None:
    result = runner.invoke(cli_app, ["cmd"], obj=obj)
    assert result.exit_code == 2
    assert "Error: no instance found; create one first." in result.stderr


def test_instance_identifier_option_implicit(
    runner: CliRunner,
    obj: Obj,
    instance: Instance,
    cli_app: click.Group,
    pg_version: str,
) -> None:
    result = runner.invoke(cli_app, ["cmd"], obj=obj)
    assert result.exit_code == 0
    assert result.stdout == f"command on 'test' ({pg_version})\n"


def test_instance_identifier_option_ambiguous(
    runner: CliRunner,
    obj: Obj,
    instance: Instance,
    instance2: Instance,
    cli_app: click.Group,
) -> None:
    result = runner.invoke(cli_app, ["cmd"], obj=obj)
    assert result.exit_code == 2
    assert (
        "Error: several instances found; option '-i' / '--instance' is required."
        in result.stderr
    )


def test_instance_commands_completion(runner: CliRunner, obj: Obj) -> None:
    group = instance_cli.cli
    assert group.name
    comp = ShellComplete(group, {"obj": obj}, group.name, "_CLICK_COMPLETE")
    commands = [c.value for c in comp.get_completions([], "")]
    assert commands == [
        "alter",
        "backup",
        "backups",
        "create",
        "drop",
        "env",
        "exec",
        "get",
        "list",
        "logs",
        "privileges",
        "promote",
        "reload",
        "restart",
        "restore",
        "shell",
        "start",
        "status",
        "stop",
        "upgrade",
    ]


def test_obj(monkeypatch: pytest.MonkeyPatch) -> None:
    with monkeypatch.context() as m:
        m.setenv("SETTINGS", json.dumps({"invalid": None}))
        with pytest.raises(click.ClickException, match="invalid site settings"):
            _site._load_settings()


def test_obj_as_root(run_as_non_root: MagicMock) -> None:
    run_as_non_root.return_value = True
    with pytest.raises(click.ClickException, match="unsupported operation"):
        _site._load_settings()


def test_cli(runner: CliRunner, obj: Obj) -> None:
    # invoke the CLI with no option, sanity check
    result = runner.invoke(cli, obj=obj)
    assert result.exit_code == 0
    assert result.stdout.splitlines()[0] == "Usage: cli [OPTIONS] COMMAND [ARGS]..."


def test_non_interactive(runner: CliRunner, settings: Settings) -> None:
    @cli.command("confirmme")
    def confirm_me() -> None:
        if not ui.confirm("Confirm?", default=True):
            raise click.Abort()
        print("confirmed")

    result = runner.invoke(cli, ["confirmme"], input="n\n")
    assert result.exit_code == 1 and "Aborted!" in result.stderr

    result = runner.invoke(cli, ["--non-interactive", "confirmme"])
    assert result.exit_code == 0 and "confirmed" in result.stdout


def test_version(runner: CliRunner, obj: Obj) -> None:
    result = runner.invoke(cli, ["--version"], obj=obj)
    assert re.match(r"pglift version (\d\.).*", result.stdout)


def test_site_settings_yaml(runner: CliRunner, settings: Settings, obj: Obj) -> None:
    s = settings.model_dump(mode="json")
    assert s["powa"]
    with set_console_width(500):
        result = runner.invoke(cli, ["site-settings"], obj=obj)
    assert result.exit_code == 0, result.stderr
    assert yaml.safe_load(result.output) == s


def test_site_settings_json(runner: CliRunner, settings: Settings, obj: Obj) -> None:
    s = settings.model_dump(mode="json")
    result = runner.invoke(cli, ["site-settings", "-o", "json"], obj=obj)
    assert result.exit_code == 0, result.stderr
    assert json.loads(result.output) == s


def test_site_settings_defaults(
    runner: CliRunner,
    settings: Settings,
    obj: Obj,
    monkeypatch: pytest.MonkeyPatch,
    default_settings: Settings,
) -> None:
    with monkeypatch.context() as m:
        m.setenv("PREFIX", "/srv/pglift")
        result = runner.invoke(
            cli, ["site-settings", "-o", "json", "--defaults"], obj=obj
        )
        assert result.exit_code == 0, result.stderr
    defaults_s = json.loads(result.output)
    assert defaults_s["prefix"] == str(default_settings.prefix)
    assert defaults_s["powa"] is None


def test_site_settings_no_defaults(
    runner: CliRunner,
    settings: Settings,
    obj: Obj,
    monkeypatch: pytest.MonkeyPatch,
) -> None:
    s = settings.model_dump(mode="json")
    assert s["powa"] is not None
    with monkeypatch.context() as m:
        m.setenv("PREFIX", "/srv/pglift")
        result = runner.invoke(
            cli, ["site-settings", "-o", "json", "--no-defaults"], obj=obj
        )
        assert result.exit_code == 0, result.stderr
    no_defaults_s = json.loads(result.output)
    # lock_file is not explicitly defined, but is computed from environment
    assert no_defaults_s["cli"]["lock_file"] == s["cli"]["lock_file"]
    # powa is defined explicitly (empty)
    assert no_defaults_s["powa"] != s["powa"]
    # systemd contains values computed from environment (unit_path) and some
    # not explicitly defined (sudo)
    assert "sudo" not in no_defaults_s["systemd"] and "sudo" in s["systemd"]
    assert no_defaults_s["systemd"]["unit_path"] == s["systemd"]["unit_path"]


def test_site_settings_schema(runner: CliRunner, settings: Settings, obj: Obj) -> None:
    result = runner.invoke(cli, ["site-settings", "--schema", "-o", "json"], obj=obj)
    assert result.exit_code == 0, result.stderr
    schema = json.loads(result.output)
    schema.pop("title")
    expected = settings.model_json_schema()
    expected.pop("title")
    assert schema == expected


@pytest.mark.parametrize("shell", ["bash", "fish", "zsh"])
def test_completion(runner: CliRunner, shell: str) -> None:
    result = runner.invoke(cli, ["--completion", shell])
    assert result.exit_code == 0, result
    assert "_pglift_completion" in result.output


@pytest.mark.parametrize(
    "args, command, objtype",
    [
        ("instance --ansible-argspec", cli, "instance"),
        ("role --ansible-argspec", cli, "role"),
        ("database --ansible-argspec", cli, "database"),
        ("--ansible-argspec", prometheus.cli, "postgresexporter"),
    ],
)
def test_argspec(
    datadir: Path,
    runner: CliRunner,
    obj: Obj,
    args: str,
    command: click.Command,
    objtype: str,
    write_changes: bool,
) -> None:
    result = runner.invoke(command, args.split(), obj=obj)
    data = json.loads(result.stdout)
    fpath = datadir / f"ansible-argspec-{objtype}.json"
    expected = json.loads(fpath.read_text())
    assert data == expected


def test_instance_schema(runner: CliRunner, obj: Obj) -> None:
    result = runner.invoke(cli, ["instance", "--schema"], obj=obj)
    schema = json.loads(result.output)
    assert schema["title"] == "Instance"
    assert schema["required"] == ["name", "pgbackrest"]


@pytest.mark.usefixtures("installed")
def test_instance_shell_var_missing(
    runner: CliRunner, instance: Instance, obj: Obj, monkeypatch: pytest.MonkeyPatch
) -> None:
    with patch("os.execle", autospec=True) as execle, monkeypatch.context() as m:
        m.delenv("SHELL", raising=False)
        r = runner.invoke(
            cli,
            ["instance", "shell", instance.name],
            obj=obj,
        )
    assert not execle.called
    assert r.exit_code == 2
    assert (
        "Error: SHELL environment variable not found; try to use --shell option"
        in r.stderr
    )


@pytest.mark.usefixtures("installed")
def test_instance_shell(
    runner: CliRunner, instance: Instance, obj: Obj, monkeypatch: pytest.MonkeyPatch
) -> None:
    with patch("os.execle", autospec=True) as execle, monkeypatch.context() as m:
        m.setenv("SHELL", "fooshell")
        runner.invoke(
            cli,
            ["instance", "shell", instance.name],
            obj=obj,
        )
    path, arg, env = execle.call_args.args
    assert path == arg == "fooshell"
    assert env["PGHOST"] == "/socks"


@pytest.mark.usefixtures("installed")
def test_pgconf_edit(
    runner: CliRunner, obj: Obj, instance: Instance, postgresql_conf: str
) -> None:
    manifest = interface.Instance(
        name="test",
        settings={
            "unix_socket_directories": "/socks",
            "cluster_name": "unittests",
        },
    )
    with (
        patch("click.edit", return_value="bonjour = bonsoir\n", autospec=True) as edit,
        patch.object(
            postgresql, "status", return_value=Status.running, autospec=True
        ) as status,
        patch.object(instances, "_get", return_value=manifest, autospec=True) as _get,
        patch.object(
            instances,
            "configure",
            return_value={"bonjour": ("on", "'matin")},
            autospec=True,
        ) as configure,
    ):
        result = runner.invoke(
            cli,
            ["pgconf", f"--instance={instance}", "edit"],
            obj=obj,
        )
    assert result.exit_code == 0, result.stderr
    status.assert_called_once_with(instance)
    _get.assert_called_once_with(instance, Status.running)
    edit.assert_called_once_with(text=postgresql_conf)
    assert manifest.settings == {"bonjour": "bonsoir"}
    configure.assert_called_once_with(instance, manifest, _is_running=True)
    assert result.stderr == "bonjour: on -> 'matin\n"


@pytest.mark.usefixtures("installed")
def test_pgconf_edit_no_change(
    runner: CliRunner, obj: Obj, instance: Instance, postgresql_conf: str
) -> None:
    with (
        patch("click.edit", return_value=None, autospec=True) as edit,
        patch.object(postgresql, "status", autospec=True) as status,
        patch.object(instances, "_get", autospec=True) as _get,
        patch.object(instances, "configure", autospec=True) as configure,
    ):
        result = runner.invoke(
            cli, ["pgconf", f"--instance={instance}", "edit"], obj=obj
        )
    edit.assert_called_once_with(text=postgresql_conf)
    assert not status.called
    assert not _get.called
    assert not configure.called
    assert result.stderr == "no change\n"


def test_role_schema(runner: CliRunner, obj: Obj) -> None:
    result = runner.invoke(cli, ["role", "--schema"], obj=obj)
    schema = json.loads(result.output)
    assert schema["title"] == "Role"
    assert schema["required"] == ["name"]


def test_database_schema(runner: CliRunner, obj: Obj) -> None:
    result = runner.invoke(cli, ["database", "--schema"], obj=obj)
    schema = json.loads(result.output)
    assert schema["title"] == "Database"
    assert schema["description"] == "PostgreSQL database"


def test_postgres(
    runner: CliRunner, instance: Instance, obj: Obj, settings: Settings
) -> None:
    result = runner.invoke(postgres.cli, ["no-suchinstance"], obj=obj)
    assert result.exit_code == 2
    assert "Invalid value for 'INSTANCE': 'no' is not a valid" in result.stderr

    result = runner.invoke(postgres.cli, [instance.name], obj=obj)
    assert result.exit_code == 2
    assert (
        "Invalid value for 'INSTANCE': invalid qualified name 'test'" in result.stderr
    )

    with patch("pglift.cli.postgres.execute_program", autospec=True) as p:
        result = runner.invoke(
            postgres.cli, [f"{instance.version}-{instance.name}"], obj=obj
        )
    assert result.exit_code == 0
    p.assert_called_once_with(
        [str(instance.bindir / "postgres"), "-D", str(instance.datadir)]
    )


def test_postgres_exporter_schema(runner: CliRunner, obj: Obj) -> None:
    result = runner.invoke(prometheus.cli, ["--schema"], obj=obj)
    schema = json.loads(result.output)
    assert schema["title"] == "PostgresExporter"
    assert schema["description"] == "Prometheus postgres_exporter service."


@pytest.mark.usefixtures("installed")
def test_patroni_logs(
    runner: CliRunner, obj: Obj, settings: Settings, instance: system.Instance
) -> None:
    with patch(
        "pglift.patroni.impl.logs", return_value=["l1\n", "l2\n"], autospec=True
    ) as logs:
        result = runner.invoke(patroni.cli, ["-i", str(instance), "logs"], obj=obj)
    assert result.exit_code == 0, click_result_traceback(result)
    logs.assert_called_once_with(instance.qualname, settings.patroni)
    assert result.output == "l1\nl2\n"
