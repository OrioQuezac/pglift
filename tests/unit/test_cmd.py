# SPDX-FileCopyrightText: 2021 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

from __future__ import annotations

import logging
import subprocess
import sys
from pathlib import Path
from unittest.mock import patch

import pytest

from pglift import cmd, exceptions
from pglift.exceptions import CommandError, SystemError
from pglift.types import Status


@pytest.mark.anyio
async def test_asyncio_run(tmp_path: Path, caplog: pytest.LogCaptureFixture) -> None:
    caplog.clear()
    with caplog.at_level(logging.DEBUG, logger=cmd.__name__):
        r = await cmd.asyncio_run(["true"], input="a")
    assert r.returncode == 0
    assert not r.stderr and not r.stdout
    assert caplog.messages == ["true"]

    caplog.clear()
    with caplog.at_level(logging.DEBUG, logger=cmd.__name__):
        assert (await cmd.asyncio_run(["echo", "ahah"])).stdout == "ahah\n"
    assert caplog.messages == ["echo ahah"]

    caplog.clear()
    with caplog.at_level(logging.DEBUG, logger=cmd.__name__):
        r = await cmd.asyncio_run(
            ["cat", "nosuchfile"], env={"LANG": "C"}, cwd=tmp_path
        )
    assert r.stderr == "cat: nosuchfile: No such file or directory\n"
    assert caplog.messages == [
        "cat nosuchfile",
        "cat: cat: nosuchfile: No such file or directory",
    ]

    caplog.clear()
    with (
        caplog.at_level(logging.DEBUG, logger=cmd.__name__),
        pytest.raises(
            exceptions.CommandError,
            match=r"Command .* returned non-zero exit status 1",
        ),
    ):
        await cmd.asyncio_run(["cat", "doesnotexist"], check=True, cwd=tmp_path)
    assert caplog.messages == [
        "cat doesnotexist",
        "cat: cat: doesnotexist: No such file or directory",
    ]

    caplog.clear()
    with caplog.at_level(logging.DEBUG, logger=cmd.__name__):
        with pytest.raises(subprocess.TimeoutExpired):
            await cmd.asyncio_run(["sleep", "0.1"], timeout=0.01)
    assert caplog.messages == ["sleep 0.1"]

    caplog.clear()
    with (
        caplog.at_level(logging.DEBUG, logger=cmd.__name__),
        pytest.raises(
            exceptions.FileNotFoundError, match=r"program from command 'nosuchcommand"
        ),
    ):
        await cmd.asyncio_run(["nosuchcommand", "x", "y", "-v"])
    assert caplog.messages == ["nosuchcommand x y -v"]


def test_execute_program(caplog: pytest.LogCaptureFixture, tmp_path: Path) -> None:
    command = ["/c", "m", "d"]
    with (
        patch("os.execve", autospec=True) as execve,
        patch("os.execv", autospec=True) as execv,
    ):
        cmd.execute_program(command, env={"X": "Y"})
        execve.assert_called_once_with("/c", command, {"X": "Y"})
        assert not execv.called
    with (
        patch("os.execve", autospec=True) as execve,
        patch("os.execv", autospec=True) as execv,
        caplog.at_level(logging.DEBUG, logger="pglift.cmd"),
    ):
        cmd.execute_program(command)
        execv.assert_called_once_with("/c", command)
        assert not execve.called
    assert "executing program '/c m d'" in caplog.records[0].message


@pytest.mark.anyio
async def test_asyncio_start_program_terminate_program_status_program(
    caplog: pytest.LogCaptureFixture, tmp_path: Path
) -> None:
    pidfile = tmp_path / "sleep" / "pid"
    async with cmd.asyncio_start_program(
        ["sleep", "10"], pidfile, timeout=0.01, env={"X_DEBUG": "1"}
    ) as proc:
        pass
    with pidfile.open() as f:
        pid = f.read()
    assert proc.pid == int(pid)

    procdir = Path("/proc") / pid
    assert procdir.exists()
    assert "sleep\x0010\x00" in (procdir / "cmdline").read_text()
    assert "X_DEBUG" in (procdir / "environ").read_text()

    assert cmd.status_program(pidfile) == Status.running

    with pidfile.open("a") as f:
        f.write("\nextra\ninformation\nignored")
    assert cmd.status_program(pidfile) == Status.running

    with pytest.raises(SystemError, match="running already"):
        async with cmd.asyncio_start_program(["sleep", "10"], pidfile):
            pass

    cmd.terminate_program(pidfile)
    r = subprocess.run(["pgrep", pid], check=False)
    assert r.returncode == 1
    await proc.communicate()
    assert proc.returncode == -15

    assert not pidfile.exists()
    assert cmd.status_program(pidfile) == Status.not_running

    pidfile = tmp_path / "invalid.pid"
    pidfile.write_text("innnnvaaaaaaaaaaliiiiiiiiiiid")
    assert cmd.status_program(pidfile) == Status.not_running
    caplog.clear()
    with (
        pytest.raises(CommandError) as excinfo,
        caplog.at_level(logging.DEBUG, logger=__name__),
    ):
        async with cmd.asyncio_start_program(
            ["sleep", "well"], pidfile, env={"LANG": "C", "LC_ALL": "C"}
        ):
            pass
    assert not pidfile.exists()
    assert "sleep is supposed to be running" in caplog.records[0].message
    assert "sleep: invalid time interval 'well'" in caplog.records[2].message
    assert "sleep: invalid time interval 'well'" in excinfo.value.stderr

    pidfile = tmp_path / "notfound"
    caplog.clear()
    with caplog.at_level(logging.WARNING, logger="pglift.cmd"):
        cmd.terminate_program(pidfile)
    assert f"program from {pidfile} not running" in caplog.records[0].message


@pytest.mark.anyio
async def test_asyncio_start_program_exception(
    tmp_path: Path, caplog: pytest.LogCaptureFixture
) -> None:
    pidfile = tmp_path / "pid"
    pyprog = tmp_path / "prog.py"
    pyprog.write_text(
        "\n".join(
            [
                "import logging, time, signal, sys",
                "signal.signal(",
                "   signal.SIGTERM,",
                "   lambda signum, frame: logging.error('got signal %d', signum),",
                ")",
                "s = float(sys.argv[1])",
                "logging.warning('sleeping %.1fs', s)",
                "time.sleep(s)",
            ]
        )
    )
    with (
        pytest.raises(ValueError, match="expected"),
        caplog.at_level(logging.DEBUG, logger="pglift.cmd"),
    ):
        async with cmd.asyncio_start_program(
            [sys.executable, str(pyprog), "0.2"], pidfile, timeout=0.1
        ) as proc:
            assert pidfile.exists()
            assert proc.returncode is None
            raise ValueError("expected")
    messages = caplog.messages
    startuplog, pylog, signallog, termlog = messages
    assert startuplog.startswith("starting program '")
    assert "sleeping 0.2s" in pylog
    assert "got signal 15" in signallog
    assert "terminated program" in termlog
    assert not pidfile.exists()
