# SPDX-FileCopyrightText: 2021 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

from __future__ import annotations

import datetime
import fnmatch
import logging
import re
from collections.abc import AsyncIterator, Awaitable
from functools import partial

import psycopg
import pytest
from tenacity import Retrying, retry
from tenacity.retry import retry_if_exception_type
from tenacity.stop import stop_after_attempt
from tenacity.wait import wait_fixed

from pglift import databases, db, exceptions, instances, postgresql
from pglift.models import interface, system
from pglift.settings import Settings
from pglift.settings._postgresql import PostgreSQLVersion

from . import async_connect, connect, execute
from .conftest import DatabaseFactory, Factory, RoleFactory, TablespaceFactory

pytestmark = pytest.mark.anyio


@pytest.fixture(scope="module", autouse=True)
async def _postgresql_running(instance: system.Instance) -> None:
    if not await postgresql.is_running(instance):
        pytest.fail("instance is not running")


@pytest.fixture
async def standby_instance_stopped(
    standby_instance: system.Instance,
) -> AsyncIterator[None]:
    await instances.stop(standby_instance)
    try:
        yield
    finally:
        await instances.start(standby_instance)


@pytest.fixture
def pg_database_owner(pg_version: str, surole_name: str) -> str:
    return "pg_database_owner" if pg_version >= "14" else surole_name


async def test_exists(
    instance: system.Instance, database_factory: DatabaseFactory
) -> None:
    assert not await databases.exists(instance, "absent")
    database_factory("present")
    assert await databases.exists(instance, "present")


async def test_apply(
    instance: system.Instance,
    database_factory: DatabaseFactory,
    role_factory: RoleFactory,
    pg_database_owner: str,
    surole_name: str,
) -> None:
    r = execute(
        instance,
        "SELECT default_version FROM pg_available_extensions WHERE name='hstore'",
    )
    assert r is not None
    default_version = r[0]["default_version"]

    dbname = "applyme"
    database = interface.Database(
        name=dbname,
        settings={"work_mem": "1MB"},
        extensions=[{"name": "hstore", "version": default_version}],
        schemas=[{"name": "myapp"}, {"name": "my_schema"}],
        tablespace="pg_default",
    )
    assert not await databases.exists(instance, database.name)

    assert (await databases.apply(instance, database)).change_state == "created"

    db = await databases.get(instance, dbname)
    assert db.settings == {"work_mem": "1MB"}

    assert db.schemas == [
        interface.Schema(name="my_schema", owner=surole_name),
        interface.Schema(name="myapp", owner=surole_name),
        interface.Schema(name="public", owner=pg_database_owner),
    ]

    assert db.extensions == [
        interface.Extension(name="hstore", schema="public", version=default_version),  # type: ignore[call-arg]
    ]

    assert (await databases.apply(instance, database)).change_state is None  # no-op

    database = interface.Database(
        name=dbname,
        owner=surole_name,
        settings={"work_mem": "1MB"},
        schemas=[
            interface.Schema(name="my_schema", owner=surole_name),
            interface.Schema(name="myapp", owner=surole_name),
            interface.Schema(name="public", owner=pg_database_owner),
        ],
        extensions=[
            {"name": "hstore", "schema": "my_schema", "version": default_version},
        ],
        tablespace="pg_default",
    )
    assert (await databases.apply(instance, database)).change_state == "changed"
    assert await databases.get(instance, dbname) == database

    database = interface.Database(name=dbname, state="absent")
    assert await databases.exists(instance, dbname)
    assert (await databases.apply(instance, database)).change_state == "dropped"
    assert not await databases.exists(instance, dbname)


async def test_apply_change_owner(
    instance: system.Instance,
    database_factory: DatabaseFactory,
    role_factory: RoleFactory,
    pg_database_owner: str,
    surole_name: str,
) -> None:
    database_factory("apply")
    database = interface.Database(name="apply")
    assert (await databases.apply(instance, database)).change_state is None  # no-op
    assert (await databases.get(instance, "apply")).owner == surole_name

    role_factory("dbapply")
    database = interface.Database(
        name="apply",
        owner="dbapply",
        schemas=[interface.Schema(name="public", owner=pg_database_owner)],
        tablespace="pg_default",
    )
    assert (await databases.apply(instance, database)).change_state == "changed"
    try:
        assert await databases.get(instance, "apply") == database
    finally:
        await databases.drop(instance, interface.DatabaseDropped(name="apply"))


async def test_apply_change_tablespace(
    instance: system.Instance,
    database_factory: DatabaseFactory,
    tablespace_factory: TablespaceFactory,
    standby_instance_stopped: system.Instance,
    pg_database_owner: str,
    surole_name: str,
) -> None:
    database_factory("apply")
    database = interface.Database(name="apply")
    assert (await databases.apply(instance, database)).change_state is None  # no-op

    tablespace_factory("dbs2")
    database = interface.Database(
        name="apply",
        owner=surole_name,
        tablespace="dbs2",
        schemas=[interface.Schema(name="public", owner=pg_database_owner)],
    )
    assert (await databases.apply(instance, database)).change_state == "changed"
    try:
        assert await databases.get(instance, "apply") == database
    finally:
        await databases.drop(instance, interface.DatabaseDropped(name="apply"))


async def test_apply_update_schemas(
    instance: system.Instance,
    database_factory: DatabaseFactory,
    role_factory: RoleFactory,
    pg_database_owner: str,
    surole_name: str,
) -> None:
    database_factory("db3")
    execute(instance, "CREATE SCHEMA my_schema", fetch=False, dbname="db3")

    assert (await databases.get(instance, "db3")).schemas == [
        interface.Schema(name="my_schema", owner=surole_name),
        interface.Schema(name="public", owner=pg_database_owner),
    ]

    role_factory("schemauser")
    database = interface.Database(
        name="db3",
        schemas=[
            interface.Schema(name="my_schema", owner="schemauser"),
        ],
    )
    assert (await databases.apply(instance, database)).change_state == "changed"
    assert (await databases.get(instance, "db3")).schemas == [
        interface.Schema(name="my_schema", owner="schemauser"),
        interface.Schema(name="public", owner=pg_database_owner),
    ]

    database = interface.Database(
        name="db3",
        schemas=[
            interface.Schema(name="my_schema", state="absent", owner=surole_name),
        ],
    )
    assert (await databases.apply(instance, database)).change_state == "changed"

    assert (await databases.get(instance, "db3")).schemas == [
        interface.Schema(name="public", owner=pg_database_owner)
    ]


async def test_apply_update_extensions(
    instance: system.Instance, database_factory: DatabaseFactory
) -> None:
    database_factory("db4")
    execute(
        instance,
        "CREATE SCHEMA my_schema",
        "CREATE SCHEMA second_schema",
        "CREATE EXTENSION unaccent WITH SCHEMA my_schema",
        "CREATE EXTENSION hstore WITH VERSION '1.4'",
        fetch=False,
        dbname="db4",
    )
    r = execute(
        instance,
        "SELECT name, default_version FROM pg_available_extensions",
    )
    assert r is not None
    extversions = {e["name"]: e["default_version"] for e in r}
    unaccent_version = extversions["unaccent"]
    pgss_version = extversions["pg_stat_statements"]
    hstore_version = extversions["hstore"]
    intarray_version = extversions["intarray"]

    assert (await databases.get(instance, "db4")).extensions == [
        interface.Extension(name="hstore", schema="public", version="1.4"),  # type: ignore[call-arg]
        interface.Extension(  # type: ignore[call-arg]
            name="unaccent", schema="my_schema", version=unaccent_version
        ),
    ]

    database = interface.Database(
        name="db4",
        extensions=[
            {"name": "pg_stat_statements", "schema": "my_schema"},
            {"name": "unaccent"},
            {"name": "hstore"},
            {"name": "intarray", "version": intarray_version},
        ],
    )
    assert (await databases.apply(instance, database)).change_state == "changed"
    assert (await databases.get(instance, "db4")).extensions == [
        interface.Extension(name="hstore", schema="public", version=hstore_version),  # type: ignore[call-arg]
        interface.Extension(name="intarray", schema="public", version=intarray_version),  # type: ignore[call-arg]
        interface.Extension(  # type: ignore[call-arg]
            name="pg_stat_statements", schema="my_schema", version=pgss_version
        ),
        interface.Extension(name="unaccent", schema="public", version=unaccent_version),  # type: ignore[call-arg]
    ]

    database = interface.Database(
        name="db4",
        extensions=[
            {"name": "hstore", "state": "absent"},
            {"name": "pg_stat_statements", "state": "absent"},
            {"name": "unaccent", "state": "absent"},
        ],
    )
    assert (await databases.apply(instance, database)).change_state == "changed"
    assert [
        e.model_dump(by_alias=True)
        for e in (await databases.get(instance, "db4")).extensions
    ] == [{"name": "intarray", "schema": "public", "version": intarray_version}]

    assert (await databases.apply(instance, database)).change_state is None


async def test_publications(
    instance: system.Instance,
    database_factory: DatabaseFactory,
    pg_database_owner: str,
    surole_name: str,
) -> None:
    database_factory("publisher")
    execute(
        instance,
        "CREATE TABLE things (u int)",
        "CREATE TABLE users (s int)",
        "CREATE TABLE departments (n text)",
        "CREATE PUBLICATION test FOR TABLE users, departments",
        dbname="publisher",
        fetch=False,
    )
    db = await databases.get(instance, "publisher")
    assert db.model_dump() == {
        "extensions": [],
        "name": "publisher",
        "owner": surole_name,
        "publications": [{"name": "test"}],
        "schemas": [{"name": "public", "owner": pg_database_owner}],
        "settings": None,
        "subscriptions": [],
        "tablespace": "pg_default",
    }

    database = interface.Database(
        name="publisher",
        publications=[{"name": "mypub"}],
    )
    assert (await databases.apply(instance, database)).change_state == "changed"

    (row,) = execute(
        instance,
        "SELECT puballtables FROM pg_publication WHERE pubname = 'mypub'",
        dbname="publisher",
    )
    assert row["puballtables"] is True

    db = await databases.get(instance, "publisher")
    assert db.model_dump() == {
        "extensions": [],
        "name": "publisher",
        "owner": surole_name,
        "publications": [
            {"name": "mypub"},
            {"name": "test"},
        ],
        "schemas": [{"name": "public", "owner": pg_database_owner}],
        "settings": None,
        "subscriptions": [],
        "tablespace": "pg_default",
    }

    database = interface.Database(
        name="publisher",
        publications=[{"name": "test", "state": "absent"}],
    )
    assert (await databases.apply(instance, database)).change_state == "changed"
    db = await databases.get(instance, "publisher")
    assert db.model_dump()["publications"] == [
        {"name": "mypub"},
    ]

    assert (await databases.apply(instance, database)).change_state is None


@pytest.fixture
def publisher_role() -> interface.Role:
    return interface.Role.model_validate(
        {"name": "app", "login": True, "password": "secret", "replication": True}
    )


@pytest.fixture
def published_dbname() -> str:
    return "app"


@pytest.fixture
def publication_name() -> str:
    return "pub"


@pytest.fixture
async def publisher_instance(
    settings: Settings,
    publisher_role: interface.Role,
    published_dbname: str,
    publication_name: str,
    surole_password: str | None,
    instance_factory: Factory[Awaitable[tuple[interface.Instance, system.Instance]]],
    logger: logging.Logger,
) -> system.Instance:
    _, instance = await instance_factory(
        settings.model_copy(
            update={
                "pgbackrest": None,
                "powa": None,
                "prometheus": None,
                "temboard": None,
            },
            deep=True,
        ),
        name="publisher",
        settings={
            "wal_level": "logical",
            "synchronous_commit": "remote_apply",
            "log_line_prefix": "",
        },
        # Either use surole_password which would be in password_command or set
        # a dummy password only needed at instance creation in order to get
        # --auth-local=md5 working.
        surole_password=surole_password or "super",
        auth={
            "local": "md5",
            "host": "md5",
        },
        state="started",
        roles=[publisher_role],
        databases=[
            {
                "name": published_dbname,
                "owner": publisher_role.name,
                "publications": [
                    {"name": publication_name},
                ],
            },
        ],
    )
    assert instance.config()["wal_level"] == "logical"
    execute(
        instance,
        "CREATE TABLE t (s int)",
        dbname=published_dbname,
        role=publisher_role,
        fetch=False,
    )
    try:
        for line in postgresql.logs(instance, timeout=0):
            logger.debug("publisher instance: %s", line.rstrip())
    except TimeoutError:
        pass
    return instance


async def test_subscriptions(
    publisher_role: interface.Role,
    published_dbname: str,
    publication_name: str,
    publisher_instance: system.Instance,
    role_factory: RoleFactory,
    instance: system.Instance,
    logger: logging.Logger,
    pg_database_owner: str,
) -> None:
    assert publisher_role.password is not None
    publisher_password = publisher_role.password.get_secret_value()
    connection = interface.ConnectionString(
        conninfo=db.dsn(
            publisher_instance, user=publisher_role.name, dbname=published_dbname
        ),
        password=publisher_password,
    )
    with psycopg.connect(connection.full_conninfo):
        # Make sure the connection string to be used for subscription is usable.
        pass
    subname = "subs"
    subscription = {
        "name": subname,
        "connection": connection,
        "publications": [publication_name],
        "enabled": True,
    }
    dbname = "subscriber"
    role_factory(publisher_role.name)
    target = {
        "name": dbname,
        "owner": publisher_role.name,
        "clone": {
            "dsn": f"postgresql://{publisher_role.name}:{publisher_password}@127.0.0.1:{publisher_instance.port}/{published_dbname}",
            "schema_only": True,
        },
        "subscriptions": [subscription],
    }

    @retry(reraise=True, wait=wait_fixed(1), stop=stop_after_attempt(5))
    def check_replication(expected: int) -> None:
        try:
            for line in postgresql.logs(instance, timeout=0):
                logger.debug("subscriber instance: %s", line.rstrip())
        except TimeoutError:
            pass
        for _ in range(3):
            execute(instance, "SELECT pg_sleep(1)", dbname=dbname, fetch=False)
            (row,) = execute(instance, "SELECT MAX(s) AS m FROM t", dbname=dbname)
            if (m := row["m"]) == expected:
                break
        else:
            pytest.fail(f"not replicated: {m} != {expected}")

    try:
        assert (
            await databases.apply(instance, interface.Database.model_validate(target))
        ).change_state == "created"
        actual = await databases.get(instance, dbname)
        assert actual.model_dump() == {
            "name": dbname,
            "owner": publisher_role.name,
            "settings": None,
            "schemas": [{"name": "public", "owner": pg_database_owner}],
            "extensions": [],
            "publications": [{"name": publication_name}],
            "subscriptions": [
                {
                    "name": "subs",
                    "connection": connection.model_dump(),
                    "publications": [publication_name],
                    "enabled": True,
                }
            ],
            "tablespace": "pg_default",
        }

        execute(
            publisher_instance,
            "INSERT INTO t VALUES (1), (2)",
            dbname=published_dbname,
            fetch=False,
        )
        check_replication(2)

        subscription["enabled"] = False
        assert (
            await databases.apply(instance, interface.Database.model_validate(target))
        ).change_state == "changed"
        actual = await databases.get(instance, dbname)
        assert actual.model_dump() == {
            "name": dbname,
            "owner": publisher_role.name,
            "settings": None,
            "schemas": [{"name": "public", "owner": pg_database_owner}],
            "extensions": [],
            "publications": [{"name": publication_name}],
            "subscriptions": [
                {
                    "name": "subs",
                    "connection": connection.model_dump(),
                    "publications": [publication_name],
                    "enabled": False,
                }
            ],
            "tablespace": "pg_default",
        }

        execute(
            publisher_instance,
            "INSERT INTO t VALUES (10)",
            dbname=published_dbname,
            fetch=False,
        )
        check_replication(2)

    finally:
        if await databases.exists(instance, dbname):
            subscription["state"] = "absent"
            assert (
                await databases.apply(
                    instance, interface.Database.model_validate(target)
                )
            ).change_state == "changed"
            actual = await databases.get(instance, dbname)
            assert actual.model_dump()["subscriptions"] == []

            assert (
                await databases.apply(
                    instance, interface.Database.model_validate(target)
                )
            ).change_state is None

            await databases.drop(instance, interface.DatabaseDropped(name=dbname))


@pytest.fixture
async def clonable_database(
    role_factory: RoleFactory,
    database_factory: DatabaseFactory,
    instance: system.Instance,
) -> str:
    role_factory("cloner", "LOGIN")
    database_factory("db1", owner="cloner")
    await databases.run(
        instance, "CREATE TABLE persons AS (SELECT 'bob' AS name)", dbnames=["db1"]
    )
    await databases.run(
        instance, "ALTER TABLE persons OWNER TO cloner", dbnames=["db1"]
    )
    return f"postgresql://cloner@127.0.0.1:{instance.port}/db1"


async def test_clone(
    surole_name: str,
    clonable_database: str,
    instance: system.Instance,
    caplog: pytest.LogCaptureFixture,
) -> None:
    database = interface.Database.model_validate(
        {"name": "cloned_db", "clone": {"dsn": clonable_database}}
    )
    assert not await databases.exists(instance, database.name)
    try:
        assert (await databases.apply(instance, database)).change_state == "created"
        result = execute(instance, "SELECT * FROM persons", dbname="cloned_db")
        assert result == [{"name": "bob"}]
    finally:
        await databases.drop(instance, interface.DatabaseDropped(name="cloned_db"))

    database = interface.Database.model_validate(
        {
            "name": "cloned_schema",
            "clone": {"dsn": clonable_database, "schema_only": True},
        }
    )
    assert database.clone and database.clone.schema_only
    assert not await databases.exists(instance, database.name)
    try:
        assert (await databases.apply(instance, database)).change_state == "created"
        result = execute(instance, "SELECT * FROM persons", dbname="cloned_schema")
        assert result == []
    finally:
        await databases.drop(instance, interface.DatabaseDropped(name="cloned_schema"))

    # DSN which target is a non existing database
    options = interface.CloneOptions(
        dsn=f"postgresql://{surole_name}@127.0.0.1:{instance.port}/nosuchdb"
    )
    with (
        pytest.raises(exceptions.CommandError) as cm,
        caplog.at_level(logging.DEBUG, logger="pglift.database"),
    ):
        await databases.clone("cloned", options, instance)
    expected = [
        r'pg_dump: error: .* database "nosuchdb" does not exist',
        r"pg_restore: error: input file is too short",
    ]
    for msg in caplog.messages:
        for idx, pattern in enumerate(expected[:]):
            if re.search(pattern, msg):
                del expected[idx]
                break
    if expected:
        pytest.fail(f"expected log message(s) not found: {expected}")

    assert cm.value.cmd[0] == str(instance.bindir / "pg_dump")
    assert not await databases.exists(instance, "cloned")

    # DSN which target is a non existing user
    options = interface.CloneOptions(
        dsn=f"postgresql://nosuchuser@127.0.0.1:{instance.port}/postgres"
    )
    with pytest.raises(exceptions.CommandError) as cm:
        await databases.clone("cloned", options, instance)
    assert cm.value.cmd[0] == str(instance.bindir / "pg_dump")
    assert not await databases.exists(instance, "cloned")

    # Target database does not exist
    with pytest.raises(exceptions.CommandError) as cm:
        await databases.clone("nosuchdb", database.clone, instance)
    assert cm.value.cmd[0] == str(instance.bindir / "pg_restore")
    assert not await databases.exists(instance, "nosuchdb")


async def test_get(
    instance: system.Instance,
    database_factory: DatabaseFactory,
    pg_database_owner: str,
    surole_name: str,
) -> None:
    with pytest.raises(exceptions.DatabaseNotFound, match="absent"):
        await databases.get(instance, "absent")

    database_factory("describeme")
    execute(instance, "ALTER DATABASE describeme SET work_mem TO '3MB'", fetch=False)
    execute(
        instance,
        "CREATE SCHEMA my_schema",
        "CREATE EXTENSION unaccent WITH SCHEMA my_schema",
        fetch=False,
        dbname="describeme",
    )
    database = await databases.get(instance, "describeme")
    assert database.name == "describeme"
    assert database.settings == {"work_mem": "3MB"}
    assert database.schemas == [
        interface.Schema(name="my_schema", owner=surole_name),
        interface.Schema(name="public", owner=pg_database_owner),
    ]
    r = execute(
        instance,
        "SELECT default_version FROM pg_available_extensions WHERE name='unaccent'",
    )
    assert r is not None
    default_version = r[0]["default_version"]
    assert database.extensions == [
        interface.Extension(  # type: ignore[call-arg]
            name="unaccent", schema="my_schema", version=default_version
        )
    ]


async def test_encoding(instance: system.Instance) -> None:
    async with await async_connect(instance) as conn:
        assert await databases.encoding(conn) == "UTF8"


async def test_ls(
    instance: system.Instance, database_factory: DatabaseFactory, surole_name: str
) -> None:
    database_factory("db1")
    database_factory("db2")
    dbs = await databases.ls(instance)
    dbnames = [d.name for d in dbs]
    assert "db2" in dbnames
    dbs = await databases.ls(instance, dbnames=("db1",))
    dbnames = [d.name for d in dbs]
    assert "db2" not in dbnames
    assert len(dbs) == 1
    db1 = next(d for d in dbs).model_dump()
    db1.pop("size")
    db1["tablespace"].pop("size")
    assert db1 == {
        "acls": [],
        "collation": "C",
        "ctype": "C",
        "description": None,
        "encoding": "UTF8",
        "name": "db1",
        "owner": surole_name,
        "tablespace": {"location": "", "name": "pg_default"},
    }


async def test_drop(
    instance: system.Instance, database_factory: DatabaseFactory
) -> None:
    with pytest.raises(exceptions.DatabaseNotFound, match="absent"):
        await databases.drop(instance, interface.DatabaseDropped(name="absent"))

    database_factory("dropme")
    await databases.drop(instance, interface.DatabaseDropped(name="dropme"))
    assert not await databases.exists(instance, "dropme")


async def test_drop_force(
    pg_version: str,
    instance: system.Instance,
    database_factory: DatabaseFactory,
) -> None:
    database_factory("dropme")

    if pg_version >= PostgreSQLVersion.v13:
        with connect(instance, dbname="dropme"):
            with pytest.raises(psycopg.errors.ObjectInUse):
                await databases.drop(instance, interface.DatabaseDropped(name="dropme"))
            await databases.drop(
                instance, interface.DatabaseDropped(name="dropme", force_drop=True)
            )
        assert not await databases.exists(instance, "dropme")
    else:
        with pytest.raises(
            exceptions.UnsupportedError,
            match=r"^Force drop option can't be used with PostgreSQL < 13$",
        ):
            await databases.drop(
                instance, interface.DatabaseDropped(name="dropme", force_drop=True)
            )


async def test_run(
    instance: system.Instance,
    database_factory: DatabaseFactory,
    caplog: pytest.LogCaptureFixture,
) -> None:
    database_factory("test")
    caplog.clear()
    with caplog.at_level(logging.INFO, logger="pglift"):
        result_run = await databases.run(
            instance,
            "CREATE TABLE persons AS (SELECT 'bob' AS name)",
            dbnames=["test"],
        )
    expected = [
        "CREATE TABLE persons AS",
        "SELECT 1",
    ]
    for msg in caplog.messages:
        if re.search(expected[0], msg):
            del expected[0]
        if not expected:
            break
    else:
        pytest.fail(f"expected log message(s) not found: {expected}")
    assert not result_run
    result = execute(instance, "SELECT * FROM persons", dbname="test")
    assert result == [{"name": "bob"}]
    result_run = await databases.run(
        instance,
        "SELECT * from persons",
        dbnames=["test"],
    )
    assert result_run == {"test": [{"name": "bob"}]}


async def test_run_analyze(
    instance: system.Instance, database_factory: DatabaseFactory
) -> None:
    database_factory("test")

    @retry(
        reraise=True,
        retry=retry_if_exception_type(AssertionError),
        stop=stop_after_attempt(5),
        wait=wait_fixed(1),
    )
    def last_analyze() -> datetime.datetime:
        result = execute(
            instance,
            "SELECT MIN(last_analyze) m FROM pg_stat_all_tables WHERE last_analyze IS NOT NULL",
            dbname="test",
        )[0]["m"]
        assert isinstance(result, datetime.datetime), result
        return result

    retrying = partial(
        Retrying,
        retry=retry_if_exception_type(AssertionError),
        stop=stop_after_attempt(5),
        wait=wait_fixed(0.2),
        reraise=True,
    )

    await databases.run(instance, "ANALYZE")
    previous = last_analyze()
    await databases.run(instance, "ANALYZE")
    for attempt in retrying():
        now = last_analyze()
        with attempt:
            assert now > previous
    await databases.run(instance, "ANALYZE", exclude_dbnames=["test"])
    for attempt in retrying():
        with attempt:
            assert last_analyze() == now


async def test_run_output_notices(
    instance: system.Instance, capsys: pytest.CaptureFixture[str]
) -> None:
    await databases.run(
        instance, "DO $$ BEGIN RAISE NOTICE 'foo'; END $$", dbnames=["postgres"]
    )
    captured = capsys.readouterr()
    assert captured.out == ""
    assert captured.err == "foo\n"


async def test_dump(
    instance: system.Instance, database_factory: DatabaseFactory
) -> None:
    with pytest.raises(
        psycopg.OperationalError, match='database "absent" does not exist'
    ):
        await databases.dump(instance, "absent")
    database_factory("dbtodump")
    await databases.dump(instance, "dbtodump")
    directory = instance.dumps_directory
    assert directory.exists()
    (dumpfile,) = list(directory.iterdir())
    assert fnmatch.fnmatch(str(dumpfile), "*dbtodump_*.dump"), dumpfile
