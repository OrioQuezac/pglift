Do not manipulate PostgreSQL objects (databases, roles) when upgrading an
instance through ``instance upgrade``. This resolves an issue when trying to
upgrade an instance with PoWA enabled from PostgreSQL version 14 to higher
which resulted in a failure to upgrade ``pg_stat_statements`` extension due to
reverse-dependency from ``pg_stat_kcache``.
