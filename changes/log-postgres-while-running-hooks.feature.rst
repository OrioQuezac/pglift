Forward PostgreSQL messages to pglift logger (``DEBUG`` level) while
performing setup operations on satellite components (configuration, reload,
promote).
