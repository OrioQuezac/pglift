Fix wrong invocation of ``systemctl status`` command; the command was mostly
used as a check so the impact was not important, but an actual error was
visible in ``DEBUG`` log messages.
