Remove misleading "failed to read (postgres_exporter|temboard-agent)
configuration ..." log messages (``DEBUG`` level) during instance creation.
