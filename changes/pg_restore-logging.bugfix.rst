Fix logging of ``pg_restore`` stderr (at ``DEBUG`` level) broken since pglift
1.2.0.
