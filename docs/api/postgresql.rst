.. SPDX-FileCopyrightText: 2021 Dalibo
..
.. SPDX-License-Identifier: GPL-3.0-or-later

PostgreSQL
==========

.. currentmodule:: pglift.postgresql

Module :mod:`pglift.postgresql` exposes the following API to manipulate
a PostgreSQL cluster:

.. autofunction:: status
.. autofunction:: check_status
.. autofunction:: is_ready
.. autofunction:: logs
.. autofunction:: replication_lag
