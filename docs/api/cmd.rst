.. SPDX-FileCopyrightText: 2021 Dalibo
..
.. SPDX-License-Identifier: GPL-3.0-or-later

.. currentmodule:: pglift.cmd

Command execution
=================

.. autofunction:: asyncio_run
.. autofunction:: asyncio_start_program
.. autofunction:: terminate_program
.. autofunction:: status_program
