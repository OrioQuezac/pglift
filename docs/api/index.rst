.. SPDX-FileCopyrightText: 2021 Dalibo
..
.. SPDX-License-Identifier: GPL-3.0-or-later

Python API
==========

This section documents general aspects of the Python API that are not related
to a specific operation context but may apply to several of them. It also
describes the data models used through the API.

.. warning::
   At the moment, this Python API should not be considered stable, it might
   change without notice. This documentation is mostly useful to pglift's
   developers.

.. toctree::
    :maxdepth: 2

    datamodel
    exceptions
    cmd
    conf
    instances
    postgresql
    databases
    roles
    privileges
