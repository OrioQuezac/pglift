.. SPDX-FileCopyrightText: 2021 Dalibo
..
.. SPDX-License-Identifier: GPL-3.0-or-later

Instance (libpq) environment
----------------------------

Command ``pglift instance env`` gives access to the libpq environment for
target instance.

.. code-block:: console

    $ pglift instance env 14/main
    PATH=/usr/lib/postgresql/14/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games
    PGDATA=/home/dba/.local/share/pglift/srv/pgsql/14/main/data
    PGHOST=/home/dba/.local/share/pglift/run/postgresql
    PGPASSFILE=/home/dba/.pgpass
    PGPORT=5456
    PGUSER=postgres
    PSQLRC=/home/dba/.local/share/pglift/srv/pgsql/14/main/data/.psqlrc
    PSQL_HISTORY=/home/dba/.local/share/pglift/srv/pgsql/14/main/data/.psql_history

This can be used in shell scripts. For instance:

.. code-block:: sh

    #!/bin/sh
    export $(pglift instance env main)
    psql -d db -c 'select 1'
    pg_dump -d db > db.sql

Command ``pglift instance shell`` opens a new shell within instance
environment. This can be useful if many commands need to be run in that
environment.

.. code-block:: console

    $ pglift instance shell
    Starting '/bin/bash' with 16/main environment
    $ env | grep PGDATA
    /srv/pgsql/16/main/data
    $ psql -t -c 'SHOW data_directory'
    /srv/pgsql/16/main/data
    $ exit  # get back to previous shell
    exit


.. note::
    In addition to PostgreSQL-specific (including libpq) environment
    variables, any satellite component set up alongside the instance will also
    get its environment variables included as part of ``instance env``
    command. For example, if :ref:`pgBackRest <pgbackrest-setup>` is set up,
    ``PGBACKREST_CONFIG_PATH`` and ``PGBACKREST_STANZA`` variables will be
    present.
