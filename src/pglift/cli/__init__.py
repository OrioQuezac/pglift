# SPDX-FileCopyrightText: 2024 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

import pluggy

hookimpl = pluggy.HookimplMarker(__name__)
